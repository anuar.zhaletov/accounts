package com.capgemini.accounts.controllers;

import com.capgemini.accounts.dto.AccountResponseDTO;
import com.capgemini.accounts.models.DBEntry;
import com.capgemini.accounts.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CustomerController {
  private final CustomerService customerService;

  @GetMapping(value = "customers/{customerID}")
  @ResponseBody
  public AccountResponseDTO getCustomerInfo(@PathVariable String customerID) {
    return customerService.getCustomerInfo(customerID);
  }
}
