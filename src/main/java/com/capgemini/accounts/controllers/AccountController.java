package com.capgemini.accounts.controllers;

import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.services.AccountService;
import com.capgemini.accounts.services.ValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AccountController {
  private final AccountService accountService;
  private final ValidationService validationService;

  @PostMapping(value = "accounts/create", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public void create(@RequestBody AccountDTO accountDTO) {
    validationService.validate(accountDTO);
    accountService.createAccount(accountDTO);
  }
}
