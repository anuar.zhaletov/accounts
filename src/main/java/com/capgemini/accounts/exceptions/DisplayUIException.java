package com.capgemini.accounts.exceptions;

public class DisplayUIException extends RuntimeException {
  public DisplayUIException(String message) {
    super(message);
  }
}
