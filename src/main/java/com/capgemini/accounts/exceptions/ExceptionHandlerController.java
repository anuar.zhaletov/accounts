package com.capgemini.accounts.exceptions;

import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

@Slf4j
@RestControllerAdvice
public class ExceptionHandlerController {

  @ExceptionHandler(CustomerNotFoundException.class)
  public ResponseEntity<ErrorResponse> handleCustomerNotFoundException(HttpServletResponse res, CustomerNotFoundException e) {
    ErrorResponse errorResponse = new ErrorResponse(e.getLocalizedMessage());
    log.error("Exception happened. Cause: " + e.getLocalizedMessage(), e);
    return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<ErrorResponse> handleBadRequestException(HttpServletResponse res, BadRequestException e) throws IOException {
    ErrorResponse errorResponse = new ErrorResponse(e.getLocalizedMessage());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  public ResponseEntity<ErrorResponse> handleHttpMediaTypeNotSupportedException(HttpServletResponse res, Exception e) throws IOException {
    ErrorResponse errorResponse = new ErrorResponse(e.getLocalizedMessage());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(DisplayUIException.class)
  public ResponseEntity<ErrorResponse> handleDisplayUIException(HttpServletResponse res, DisplayUIException e) {
    ErrorResponse errorResponse = new ErrorResponse(e.getLocalizedMessage());
    log.error("Exception happened. Cause: " + e.getLocalizedMessage(), e);
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> handleException(HttpServletResponse res, Exception e) throws IOException {
    ErrorResponse errorResponse = new ErrorResponse("Internal Server Error");
    log.error("Exception happened. Cause: " + e.getLocalizedMessage(), e);
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}

