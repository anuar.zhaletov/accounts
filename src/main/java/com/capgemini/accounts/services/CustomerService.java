package com.capgemini.accounts.services;

import com.capgemini.accounts.clients.TransactionRestClient;
import com.capgemini.accounts.dto.AccountResponseDTO;
import com.capgemini.accounts.models.DBEntry;
import com.capgemini.accounts.repositories.DBRepository;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
  private final DBRepository dbRepository;
  private final TransactionRestClient transactionRestClient;

  public AccountResponseDTO getCustomerInfo(@NotNull String customerId) {
    AccountResponseDTO accountResponseDTO = dbRepository.getDBEntry(customerId);
    accountResponseDTO.setTransactions(transactionRestClient.getTransactions(customerId));
    return accountResponseDTO;
  }
}
