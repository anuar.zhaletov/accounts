package com.capgemini.accounts.services;

import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.exceptions.BadRequestException;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ValidationService {
  public void validate(@NotNull AccountDTO accountDTO) {
    if (accountDTO.getCustomerID() == null) {
      throw new BadRequestException("CustomerId cannot be null.");
    }
    if (accountDTO.getInitialCredit() == null) {
      throw new BadRequestException("InitialCredit cannot be null.");
    }
    if (accountDTO.getInitialCredit().compareTo(BigDecimal.ZERO) < 0) {
      throw new BadRequestException("InitialCredit cannot be negative.");
    }
  }
}
