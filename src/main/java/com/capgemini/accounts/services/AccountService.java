package com.capgemini.accounts.services;

import com.capgemini.accounts.clients.TransactionRestClient;
import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.repositories.DBRepository;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountService {
  private final DBRepository dbRepository;
  private final TransactionRestClient transactionRestClient;

  public void createAccount(@NotNull AccountDTO accountDTO) {
    dbRepository.createAccount(accountDTO);
    transactionRestClient.saveTransaction(accountDTO);
  }
}
