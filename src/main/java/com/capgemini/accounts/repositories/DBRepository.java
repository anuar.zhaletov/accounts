package com.capgemini.accounts.repositories;

import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.dto.AccountResponseDTO;
import com.capgemini.accounts.exceptions.DisplayUIException;
import com.capgemini.accounts.exceptions.CustomerNotFoundException;
import com.capgemini.accounts.models.Account;
import com.capgemini.accounts.models.Customer;
import com.capgemini.accounts.models.DBEntry;
import com.capgemini.accounts.models.Transaction;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.*;

@Slf4j
@Repository
public class DBRepository {
  private final RestTemplate restTemplate;

  private final Map<String, DBEntry> DB = new HashMap<>();
  private final Object lock = new Object();

  public DBRepository() {
    this.restTemplate = new RestTemplate();
    DB.put("03ea1551-faf6-494e-8108-7a480f31ea8e", DBEntry.builder()
        .customer(Customer.builder().name("John").surname("Lennon").build())
        .build());
    DB.put("08b223a0-2ea2-4c51-ae02-d1cc659ac663", DBEntry.builder()
        .customer(Customer.builder().name("Paul").surname("Mccartney").build())
        .build());
    DB.put("c0b1dea5-12af-4d90-8a3b-25c3ffedbb3a", DBEntry.builder()
        .customer(Customer.builder().name("George").surname("Harrison").build())
        .build());
    DB.put("16abdc13-c9e6-4349-a76c-f3e7c08adc7b", DBEntry.builder()
        .customer(Customer.builder().name("Ringo").surname("Starr").build())
        .build());
  }

  public void createAccount(@NotNull AccountDTO accountDTO) {
    // synchronized only creating account (not reading). ConcurrentHashMap will not help here as here is multiple steps.
    // if you are using SQL Database synchronized will not require.
    synchronized (lock) {
      if (DB.get(accountDTO.getCustomerID()) == null) {
        throw new DisplayUIException("Customer with customerID: " + accountDTO.getCustomerID() + " is not found. " +
            "Cannot create account.");
      }

      DBEntry dbEntry = DB.get(accountDTO.getCustomerID());
      if (dbEntry.getAccount() != null) {
        throw new DisplayUIException("Cannot create account. Account with customerID: " + accountDTO.getCustomerID() +
            " already exists.");
      }

      DB.put(accountDTO.getCustomerID(), DBEntry.builder()
          .account(Account.builder().balance(accountDTO.getInitialCredit()).build())
          .customer(dbEntry.getCustomer())
          .build());
    }
  }

  public AccountResponseDTO getDBEntry(String customerID) {
    DBEntry dbEntry = DB.get(customerID);
    if (dbEntry == null) {
      throw new CustomerNotFoundException("Cannot find customer with customerId: " + customerID);
    }

    return AccountResponseDTO.builder()
            .customer(dbEntry.getCustomer())
            .account(dbEntry.getAccount())
            .build();
  }
}
