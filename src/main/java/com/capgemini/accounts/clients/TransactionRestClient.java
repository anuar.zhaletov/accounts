package com.capgemini.accounts.clients;

import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.models.Transaction;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionRestClient {
    private static final String FROM_IBAN = "NL86ABNA01506175536482";
    private static final String TRANSACTIONS_API_BASE_URL = "http://localhost:8081";

    private final RestTemplate restTemplate;

    public void saveTransaction(@NotNull AccountDTO accountDTO) {
        if (accountDTO.getInitialCredit().compareTo(BigDecimal.ZERO) == 0) {
            return;
        }

        List<Transaction> transactions = List.of(Transaction.builder().fromIBAN(FROM_IBAN).amount(accountDTO.getInitialCredit()).build());

        /**
         * user RestTemplate for simplicity. But of course it is better to use FeignClient, which automatically generated from
         * Swagger of transaction service.
         */
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AccountDTO> request = new HttpEntity<>(accountDTO, headers);
        try {
            // it is better to try retry mechanism with Resilience4j. Transaction Service is idempotent, so for example we can call it 5 times.
            restTemplate.postForObject(TRANSACTIONS_API_BASE_URL + "/transactions/initialcreate", request, Object.class);
        } catch (RestClientException e) {
            log.error("Failed to create initial transaction. Cause: " + e.getLocalizedMessage(), e);
        }
    }

    public List<Transaction> getTransactions(@NotNull String customerID) {
        return Arrays.asList(Objects.requireNonNull(restTemplate.getForEntity(TRANSACTIONS_API_BASE_URL + "/transactions/" + customerID, Transaction[].class).getBody()));
    }
}
