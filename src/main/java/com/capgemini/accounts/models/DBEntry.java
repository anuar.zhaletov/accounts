package com.capgemini.accounts.models;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class DBEntry {
  Account account;
  Customer customer;
}
