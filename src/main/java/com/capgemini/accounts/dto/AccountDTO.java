package com.capgemini.accounts.dto;

import com.capgemini.accounts.models.Account;
import com.capgemini.accounts.models.Customer;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class AccountDTO {
  String customerID;
  BigDecimal initialCredit;
}
