package com.capgemini.accounts.dto;

import com.capgemini.accounts.models.Account;
import com.capgemini.accounts.models.Customer;
import com.capgemini.accounts.models.Transaction;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.util.List;

@Data
@Builder
public class AccountResponseDTO {
    private Account account;
    private Customer customer;
    private List<Transaction> transactions;
}
