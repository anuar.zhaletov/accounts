Feature: An example

  Scenario: User is creating account for customer which doesn't exists
    When user creates account with customerID "CUSTOMER_ID_NOT_STORE_IN_DATABASE" and initialCredit 100
    Then error response is 500 and "Customer with customerID: CUSTOMER_ID_NOT_STORE_IN_DATABASE is not found. Cannot create account."

  Scenario: User is creating account for customer which doesn't exists
    When user creates account with customerID "03ea1551-faf6-494e-8108-7a480f31ea8e" and initialCredit -100
    Then error response is 400 and "InitialCredit cannot be negative."

  Scenario: User is creating account with zero balance
    When user creates account with customerID "08b223a0-2ea2-4c51-ae02-d1cc659ac663" and initialCredit 0
    Then account created without transaction for customerID "08b223a0-2ea2-4c51-ae02-d1cc659ac663"

  Scenario: User is creating account with positive balance
    When user creates account with customerID "c0b1dea5-12af-4d90-8a3b-25c3ffedbb3a" and initialCredit 100
    Then account created with transaction for customerID "c0b1dea5-12af-4d90-8a3b-25c3ffedbb3a"