package com.capgemini.accounts.controllers;


import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.services.AccountService;
import com.capgemini.accounts.services.ValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AccountControllerTest {
  private AccountController accountController;

  @Mock
  private AccountService accountService;

  @Mock
  private ValidationService validationService;

  @BeforeEach
  void setUp() {
    this.accountController = new AccountController(accountService, validationService);
  }

  @Test
  void testCreate() {
    AccountDTO accountDTO = AccountDTO.builder().customerID("03ea1551-faf6-494e-8108-7a480f31ea8e").initialCredit(BigDecimal.valueOf(100)).build();
    accountController.create(accountDTO);
    verify(validationService).validate(eq(accountDTO));
    verify(accountService).createAccount(eq(accountDTO));
  }
}