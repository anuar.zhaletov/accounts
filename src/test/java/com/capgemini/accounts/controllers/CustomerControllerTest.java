package com.capgemini.accounts.controllers;

import com.capgemini.accounts.dto.AccountResponseDTO;
import com.capgemini.accounts.models.Account;
import com.capgemini.accounts.models.Customer;
import com.capgemini.accounts.models.DBEntry;
import com.capgemini.accounts.models.Transaction;
import com.capgemini.accounts.services.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {
  private CustomerController customerController;

  @Mock
  private CustomerService customerService;

  @BeforeEach
  void setUp() {
    this.customerController = new CustomerController(customerService);
  }

  @Test
  void testGetCustomerInfo() {
    String customerID = "03ea1551-faf6-494e-8108-7a480f31ea8e";
    AccountResponseDTO accountDTO = AccountResponseDTO.builder().customer(Customer.builder().name("John").surname("Lennon").build())
        .account(Account.builder().balance(BigDecimal.TEN).build())
        .transactions(List.of(Transaction.builder().amount(BigDecimal.TEN).fromIBAN("IBAN").build()))
        .build();
    when(customerService.getCustomerInfo(eq(customerID))).thenReturn(accountDTO);
    assertThat(customerController.getCustomerInfo(customerID)).isEqualTo(accountDTO);
  }
}