package com.capgemini.accounts.repositories;

import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.dto.AccountResponseDTO;
import com.capgemini.accounts.exceptions.DisplayUIException;
import com.capgemini.accounts.models.DBEntry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DBRepositoryTest {
    private DBRepository dbRepository;

    @BeforeEach
    void setUp() {
        this.dbRepository = new DBRepository();
    }

    @Test
    void createAccount_ShouldThrowExceptionWhenCustomerIdIsNotInDatabase() {
        AccountDTO accountDTO = AccountDTO.builder().initialCredit(BigDecimal.valueOf(10)).customerID("NOT_STORE_CUSTOMER_ID").build();
        Exception exception = assertThrows(DisplayUIException.class, () -> dbRepository.createAccount(accountDTO));
        assertThat(exception.getMessage()).isEqualTo("Customer with customerID: NOT_STORE_CUSTOMER_ID is not found. Cannot create account.");
    }

    @Test
    void createAccount_ShouldThrowExceptionWhenAccountIsCreatedSecondTime() {
        AccountDTO accountDTO = AccountDTO.builder().initialCredit(BigDecimal.valueOf(10)).customerID("03ea1551-faf6-494e-8108-7a480f31ea8e").build();
        dbRepository.createAccount(accountDTO);
        Exception exception = assertThrows(DisplayUIException.class, () -> dbRepository.createAccount(accountDTO));
        assertThat(exception.getMessage()).isEqualTo("Cannot create account. Account with customerID: 03ea1551-faf6-494e-8108-7a480f31ea8e already exists.");
    }

    @Test
    void createAccount_ShouldStoreTransactionWhenInitialCreditIsNotZero() {
        AccountDTO accountDTO = AccountDTO.builder().initialCredit(BigDecimal.TEN).customerID("03ea1551-faf6-494e-8108-7a480f31ea8e").build();
        dbRepository.createAccount(accountDTO);
        AccountResponseDTO accountResponseDTO = dbRepository.getDBEntry("03ea1551-faf6-494e-8108-7a480f31ea8e");
        assertThat(accountResponseDTO.getAccount().getBalance()).isEqualTo(BigDecimal.TEN);
        assertThat(accountResponseDTO.getCustomer().getName()).isEqualTo("John");
        assertThat(accountResponseDTO.getCustomer().getSurname()).isEqualTo("Lennon");
    }

    @Test
    void createAccount_ShouldGetDBEntry() {
        AccountResponseDTO accountResponseDTO = dbRepository.getDBEntry("03ea1551-faf6-494e-8108-7a480f31ea8e");
        assertThat(accountResponseDTO.getAccount()).isNull();
        assertThat(accountResponseDTO.getCustomer().getName()).isEqualTo("John");
        assertThat(accountResponseDTO.getCustomer().getSurname()).isEqualTo("Lennon");
    }
}