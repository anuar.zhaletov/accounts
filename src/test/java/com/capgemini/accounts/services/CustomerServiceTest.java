package com.capgemini.accounts.services;


import com.capgemini.accounts.clients.TransactionRestClient;
import com.capgemini.accounts.dto.AccountResponseDTO;
import com.capgemini.accounts.models.Customer;
import com.capgemini.accounts.models.DBEntry;
import com.capgemini.accounts.models.Transaction;
import com.capgemini.accounts.repositories.DBRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {
  private CustomerService customerService;

  @Mock
  private DBRepository dbRepository;

  @Mock
  private TransactionRestClient transactionRestClient;

  @BeforeEach
  void setUp() {
    this.customerService = new CustomerService(dbRepository, transactionRestClient);
  }

  @Test
  void testGetCustomerInfo() {
    String customerId = "CUSTOMER_ID";
    AccountResponseDTO accountResponseDTO = AccountResponseDTO.builder().customer(Customer.builder().name("John").surname("Lennon").build()).build();
    when(dbRepository.getDBEntry(eq(customerId))).thenReturn(accountResponseDTO);
    List<Transaction> transactions = List.of(Transaction.builder().amount(BigDecimal.TEN).fromIBAN("IBAN").build());
    when(transactionRestClient.getTransactions(eq(customerId))).thenReturn(transactions);
    AccountResponseDTO customerInfo = customerService.getCustomerInfo(customerId);
    assertThat(customerInfo).isEqualTo(accountResponseDTO);
    assertThat(customerInfo.getTransactions()).isEqualTo(transactions);
  }
}