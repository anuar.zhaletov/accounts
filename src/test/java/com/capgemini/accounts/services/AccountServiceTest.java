package com.capgemini.accounts.services;


import com.capgemini.accounts.clients.TransactionRestClient;
import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.models.Transaction;
import com.capgemini.accounts.repositories.DBRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {
  private AccountService accountService;

  @Mock
  private DBRepository dbRepository;

  @Mock
  private TransactionRestClient transactionRestClient;

  @BeforeEach
  void setUp() {
    this.accountService = new AccountService(dbRepository, transactionRestClient);
  }

  @Test
  void testCreateAccount() {
    AccountDTO accountDTO = AccountDTO.builder()
        .customerID("CUSTOMER_ID")
        .initialCredit(BigDecimal.TEN)
        .build();
    accountService.createAccount(accountDTO);
    verify(dbRepository).createAccount(eq(accountDTO));
    verify(dbRepository).createAccount(eq(accountDTO));
  }
}