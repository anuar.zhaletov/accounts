package com.capgemini.accounts.services;

import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.exceptions.BadRequestException;
import com.capgemini.accounts.exceptions.DisplayUIException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidationServiceTest {
  private ValidationService validationService;

  @BeforeEach
  void setUp() {
    this.validationService = new ValidationService();
  }

  @Test
  void validateShouldThrowExceptionWhenCustomerIDIsNull() {
    AccountDTO accountDTO = AccountDTO.builder().build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validate(accountDTO));
    assertThat(exception.getMessage()).isEqualTo("CustomerId cannot be null.");
  }

  @Test
  void validateShouldThrowExceptionWhenInitialCreditIsNull() {
    AccountDTO accountDTO = AccountDTO.builder().customerID("CUSTOMER_ID").build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validate(accountDTO));
    assertThat(exception.getMessage()).isEqualTo("InitialCredit cannot be null.");
  }

  @Test
  void validateShouldThrowExceptionWhenInitialCreditIsNegative() {
    AccountDTO accountDTO = AccountDTO.builder().customerID("CUSTOMER_ID").initialCredit(BigDecimal.valueOf(-100)).build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validate(accountDTO));
    assertThat(exception.getMessage()).isEqualTo("InitialCredit cannot be negative.");
  }
}