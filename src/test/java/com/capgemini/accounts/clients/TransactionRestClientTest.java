package com.capgemini.accounts.clients;

import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.models.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransactionRestClientTest {
    private TransactionRestClient transactionRestClient;

    @Mock
    private RestTemplate restTemplate;

    @BeforeEach
    void setUp() {
        this.transactionRestClient = new TransactionRestClient(restTemplate);
    }

    @Test
    void shouldNotSaveTransactionIfInitialCreditIsZero() {
        transactionRestClient.saveTransaction(AccountDTO.builder().customerID("CUSTOMER_ID").initialCredit(BigDecimal.ZERO).build());
        verify(restTemplate, never()).postForEntity(eq("http://localhost:8081/transactions/create"), any(), any());
    }

    @Test
    void shouldNotSaveTransactionIfInitialCreditIsPositive() {
        ArgumentCaptor<HttpEntity<AccountDTO>> httpEntityArgumentCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        transactionRestClient.saveTransaction(AccountDTO.builder().customerID("CUSTOMER_ID").initialCredit(BigDecimal.TEN).build());
        verify(restTemplate).postForObject(anyString(), httpEntityArgumentCaptor.capture(), eq(Object.class));
        HttpEntity<AccountDTO> httpEntity = httpEntityArgumentCaptor.getValue();
        assertThat(httpEntity.getBody().getInitialCredit()).isEqualTo(BigDecimal.valueOf(10));
        assertThat(httpEntity.getBody().getCustomerID()).isEqualTo("CUSTOMER_ID");
    }

    @Test
    void shouldReturnTransactions() {
        ResponseEntity mockResponseEntity = mock(ResponseEntity.class);
        Transaction[] transactions = {Transaction.builder().amount(BigDecimal.TEN).fromIBAN("IBAN").build()};
        when(mockResponseEntity.getBody()).thenReturn(transactions);
        when(restTemplate.getForEntity(anyString(), eq(Transaction[].class))).thenReturn(mockResponseEntity);
        List<Transaction> actualTransactions = transactionRestClient.getTransactions("CUSTOMER_ID");
        assertThat(actualTransactions).hasSize(1);
        assertThat(actualTransactions.get(0).getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(actualTransactions.get(0).getFromIBAN()).isEqualTo("IBAN");
    }
}