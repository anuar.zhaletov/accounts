package com.capgemini;

import com.capgemini.accounts.dto.AccountDTO;
import com.capgemini.accounts.dto.AccountResponseDTO;
import com.capgemini.accounts.exceptions.ErrorResponse;
import com.capgemini.accounts.models.DBEntry;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;

import java.math.BigDecimal;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.with;
import static org.assertj.core.api.Assertions.assertThat;

public class StepDefinitions {
  private Response response;

  @Given("user creates account with customerID {string} and initialCredit {double}")
  public void userCreatesAccount(String customerId, double initialCredit) {
    AccountDTO accountDTO = AccountDTO.builder()
        .customerID(customerId)
        .initialCredit(BigDecimal.valueOf(initialCredit))
        .build();
    this.response = with()
        .header("Content-Type", "application/json")
        .body(accountDTO)
        .when().post("http://localhost:8080/accounts/create");
  }

  @Then("error response is {int} and {string}")
  public void errorResponse(int statusCode, String errorMessage) {
    response.then().statusCode(statusCode);
    assertThat(response.getBody().as(ErrorResponse.class).getMessageError()).isEqualTo(errorMessage);
    response = null;
  }

  @Then("account created without transaction for customerID {string}")
  public void accountWithoutTransactions(String customerID) {
    this.response = with()
        .when().get("http://localhost:8080/customers/" + customerID);
    response.then().statusCode(200);
    AccountResponseDTO accountResponseDTO = response.getBody().as(AccountResponseDTO.class);
    assertThat(accountResponseDTO.getCustomer().getName()).isEqualTo("Paul");
    assertThat(accountResponseDTO.getCustomer().getSurname()).isEqualTo("Mccartney");
    assertThat(accountResponseDTO.getAccount().getBalance()).isEqualTo(BigDecimal.valueOf(0.0));
    assertThat(accountResponseDTO.getTransactions()).isEmpty();
  }

  @Then("account created with transaction for customerID {string}")
  public void accountWithTransactions(String customerID) {
    this.response = with()
        .when().get("http://localhost:8080/customers/" + customerID);
    response.then().statusCode(200);
    AccountResponseDTO accountResponseDTO = response.getBody().as(AccountResponseDTO.class);
    assertThat(accountResponseDTO.getCustomer().getName()).isEqualTo("George");
    assertThat(accountResponseDTO.getCustomer().getSurname()).isEqualTo("Harrison");
    assertThat(accountResponseDTO.getAccount().getBalance()).isEqualTo(BigDecimal.valueOf(100.0));
    assertThat(accountResponseDTO.getTransactions().get(0).getAmount()).isEqualTo(BigDecimal.valueOf(100.0));
    assertThat(accountResponseDTO.getTransactions().get(0).getFromIBAN()).isEqualTo("NL86ABNA01506175536482");
  }

}
