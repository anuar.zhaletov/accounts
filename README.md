# Getting Started
To run application:
```bash 
mvn clean install -DskipTests
mvn spring-boot:run
```

To execute cucumber scenarios:
``` 
mvn test
```

Clone this as well: https://gitlab.com/anuar.zhaletov/transactions.git
Transaction Service will run on port 8081

## Documentation
Documentation of API here (Swagger-UI): http://localhost:8080/swagger-ui/index.html

### Not Happy Flow scenarios
Application can send BadRequest or other errors like: 
HttpStatus: 500
```json
{
    "message": "InitialCredit cannot be negative."
}
```

I decided to not make UI, as cucumber is testing all scenarios and imitating user.
I've put basic azure-pipelines.yaml
But of course ideally we need to have 4 environments DTAP (and 4 application.yaml per each environment).

Endpoint: POST http://localhost:8080/accounts/create 
is idempotent. So if you will send request second time, it will not change anything.

NOTE: I decided to implement transactionService (as second service) and move logic.
it took for me another 2 hours to do it.